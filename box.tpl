 * Smarty box template
 * Theme: smarty default
 *}
<div class="box">

{if $title}
  <h2>{$title}</h2>
{/if}

  <div class="content">{$content}</div>
</div>
