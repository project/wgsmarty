 * Smarty node template
 * Theme: smarty default
 *}
<div id="node-{$node->nid}" class="node{if $sticky} sticky}{/if}{if !$status} node-unpublished{/if} clear-block">

{$picture}

{if $page == 0}
  <h2><a href="{$node_url}" title="{$title}">{$title}</a></h2>
{/if}

  <div class="meta">
{if $submitted}
    <span class="submitted">{$submitted}</span>
{/if}
{if $terms}
    <span class="terms">{$terms}</span>
{/if}
  </div>

  <div class="content">
    {$content}
  </div>

{if $links}
  {$links}
{/if}

</div>
