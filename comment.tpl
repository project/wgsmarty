 * Smarty comment template
 * Theme: smarty default
 *}
<div class="comment{if $comment->new} comment-new{/if}{if $comment->status == $smarty.const.COMMENT_NOT_PUBLISHED} comment-unpublished{/if} clear-block">
  {$picture}

{if $comment->new}
  <a id="new"></a>
  <span class="new">{$new}</span>
{/if}

  <h3>{$title}</h3>

  <div class="submitted">
    {$submitted}
  </div>

  <div class="content">
    {$content}
  </div>

  {$links}
</div>
